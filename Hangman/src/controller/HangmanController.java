package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Christopher Delvalle
 */
public class HangmanController implements FileController {

    public enum GameState {
        UNINITIALIZED,
        INITIALIZED_UNMODIFIED,
        INITIALIZED_MODIFIED,
        ENDED
    }

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private GameState   gamestate;   // the state of the game being shown in the workspace
    private Text[]      progress;    // reference to the text area for the word
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private Path        workFile;

    private Text[] blanks;
    private Text[] alphabet;

    private boolean difficult;
//    private boolean hintEnabled;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void disableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(true);
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }

    public GameState getGamestate() {
        return this.gamestate;
    }

    /**
     * In the homework code given to you, we had the line
     * gamedata = new GameData(appTemplate, true);
     * This meant that the 'gamedata' variable had access to the app, but the data component of the app was still
     * the empty game data! What we need is to change this so that our 'gamedata' refers to the data component of
     * the app, instead of being a new object of type GameData. There are several ways of doing this. One of which
     * is to write (and use) the GameData#init() method.
     */
    public void start() {
        gamedata = (GameData) appTemplate.getDataComponent();
        success = false;
        discovered = 0;

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

        gamedata.init();
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);


        gameWorkspace.getHintButton().setVisible(true); // make hint button visible


        gameWorkspace.getHintButton().setOnMouseClicked(e -> {
            this.hint();
            gameWorkspace.getHintButton().setDisable(true);
            gamedata.setHintEnabled(false);
        });

        initWordGraphics(guessedLetters);
        gameWorkspace.getHintButton().setVisible(difficult);
        gamedata.setHintEnabled(true);
        play();
    }

    private void end() {
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameButton.setDisable(true);
        setGameState(GameState.ENDED);
        appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
        Platform.runLater(() -> {
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            String                    endMessage = manager.getPropertyValue(success ? GAME_WON_MESSAGE : GAME_LOST_MESSAGE);
            if (!success) {
//                endMessage += String.format(" (the word was \"%s\")", gamedata.getTargetWord());
                for (int i = 0; i < progress.length; i++) {
                    if(!gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0))){
                        blanks[i].setText(progress[i].getText());
//                        blanks[i].setOpacity(1);

                        javafx.scene.paint.Color p = javafx.scene.paint.Color.rgb(255, 0, 0);
                        blanks[i].setFill(p);
//                        blanks[i].set


                    }

                }
            }
            if (dialog.isShowing())
                dialog.toFront();
            else
                dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), endMessage);
        });
    }

    private void initWordGraphics(HBox guessedLetters) {
        //----<

        HBox letterBox = new HBox();
        HBox targetBox = new HBox();
        //---->
        char[] targetword = gamedata.getTargetWord().toCharArray();
//difficulty determiner
        int difficulty = 0;
        for(int i = 0; i<26;i++){
            char c = (char)('a'+i);
            for(int j = 0; j<targetword.length;j++){
                if(c==targetword[j]){
                    difficulty++;
                    j=targetword.length;
                }
            }
        }
        if(difficulty>7) {
//            System.out.println("d:"+difficulty);
            difficult = true;
        }
        else{
            difficult = false;
        }

//
        progress = new Text[targetword.length];
        blanks = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            blanks[i] = new Text("▢");
//            blanks[i].setUnderline(True);
//            progress[i].setVisible(false);
        }
        //----<
        alphabet = new Text[26];
        for (int i = 0; i < alphabet.length; i++) {
            alphabet[i] = new Text(Character.toString((char)('a'+i)));

        }
        letterBox.getChildren().addAll(alphabet);
        targetBox.getChildren().addAll(blanks);

        BorderPane letterPane = new BorderPane();
        letterPane.setTop(targetBox);
        letterPane.setBottom(letterBox);

        //---->

        guessedLetters.getChildren().addAll(letterPane);
    }

    public void play() {
        disableGameButton();
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    char guess = event.getCharacter().charAt(0);
                    if (!alreadyGuessed(guess)&&Character.isAlphabetic(guess)) {
                        boolean goodguess = false;

                        alphabet[guess-'a'].setStrikethrough(true); // strikethrough when you do shit
//                        alphabet[guess-'a'].setOpacity(.3); // make it harder to see when already chosen
                        javafx.scene.paint.Color p;// = javafx.scene.paint.Color.rgb(0, 0, 255);
//                        alphabet[guess-'a'].setFill(p);
//                        alphabet[guess-'a'].set
                        for (int i = 0; i < progress.length; i++) {
                            if (gamedata.getTargetWord().charAt(i) == guess) {
                                blanks[i].setText(progress[i].getText());
                                p = javafx.scene.paint.Color.rgb(0, 200, 0); //maybe remove this
                                alphabet[guess-'a'].setFill(p);
                                gamedata.addGoodGuess(guess);
                                goodguess = true;
                                discovered++;
                            }
                        }
                        if (!goodguess) {
                            gamedata.addBadGuess(guess);
                            p = javafx.scene.paint.Color.rgb(255, 0, 0); //maybe remove this
                            alphabet[guess-'a'].setFill(p);
                            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
                            gameWorkspace.updateHangman(gamedata.getRemainingGuesses());
                        }
                        success = (discovered == progress.length);
                        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                    }
                    setGameState(GameState.INITIALIZED_MODIFIED);

                    if(gamedata.getRemainingGuesses() <=0 || success) {
                        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
                        gameWorkspace.getHintButton().setDisable(true);
                    }
                });
                if (gamedata.getRemainingGuesses() <= 0 || success)
                    stop();
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private void restoreGUI() {//do the things
        disableGameButton();
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize();

        HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        restoreWordGraphics(guessedLetters);
        if(!gamedata.getHintEnabled())
            gamedata.decrementRemaining();//decrement remaining if hint disabled
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);


        gameWorkspace.getHintButton().setVisible(difficult); // make hint button visible
        gameWorkspace.getHintButton().setOnMouseClicked(e -> {
            this.hint();
            gameWorkspace.getHintButton().setDisable(true);
            gamedata.setHintEnabled(false);
        });
        gameWorkspace.getHintButton().setDisable(!gamedata.getHintEnabled());





//loading <<
        success = false;
        gameWorkspace.updateHangman(10);
        gameWorkspace.updateHangman(gamedata.getRemainingGuesses());
        play();
    }

    private void restoreWordGraphics(HBox guessedLetters) {
        HBox letterBox = new HBox();
        HBox targetBox = new HBox();



        char[] targetword = gamedata.getTargetWord().toCharArray();

        discovered = 0;
        progress = new Text[targetword.length];
        blanks = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
//            progress[i].setVisible(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)));
            blanks[i] = new Text("▢");

            if(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)))
                blanks[i].setText(progress[i].getText());
            if (blanks[i].getText().equals(progress[i].getText()))
                discovered++;
        }

        alphabet = new Text[26];
        for (int i = 0; i < alphabet.length; i++) {
            alphabet[i] = new Text(Character.toString((char)('a'+i)));
            if(alreadyGuessed(alphabet[i].getText().charAt(0))){
                alphabet[alphabet[i].getText().charAt(0)-'a'].setStrikethrough(true); // strikethrough when you do shit
//                alphabet[alphabet[i].getText().charAt(0)-'a'].setOpacity(.3);
                javafx.scene.paint.Color p;
                if(gamedata.getGoodGuesses().contains(alphabet[i].getText().charAt(0))){
                    p = javafx.scene.paint.Color.rgb(0, 200, 0);
                }
                else{
                    p = javafx.scene.paint.Color.rgb(255, 0, 0);
                }
                alphabet[alphabet[i].getText().charAt(0)-'a'].setFill(p);
            }
        }


        //----<

        letterBox.getChildren().addAll(alphabet);
        targetBox.getChildren().addAll(blanks);

        BorderPane letterPane = new BorderPane();
        letterPane.setTop(targetBox);
        letterPane.setBottom(letterBox);

        //---->


        int difficulty = 0;
        for(int i = 0; i<26;i++){
            char c = (char)('a'+i);
            for(int j = 0; j<targetword.length;j++){
                if(c==targetword[j]){
                    difficulty++;
//                    System.out.println(difficulty);
                    j=targetword.length;
                }
            }
        }
        if(difficulty>7) {
//            System.out.println("d:"+difficulty);
            difficult = true;
        }
        else{
            difficult = false;
        }


//        gamedata.setHintEnabled(difficult);

        guessedLetters.getChildren().addAll(letterPane);
    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize();
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

            gameWorkspace.updateHangman(10);
            enableGameButton();
        }
        if (gamestate.equals(GameState.ENDED)) {
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            FileChooser filechooser = new FileChooser();
            Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
//            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                    String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null)
                save(selectedFile.toPath());
        } else
            save(workFile);
    }

    @Override
    public void handleLoadRequest() throws IOException {
        boolean load = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            load = promptToSave();
        if (load) {
            PropertyManager propertyManager = PropertyManager.getManager();
            FileChooser     filechooser     = new FileChooser();
            Path            appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path            targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
//            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                    String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null && selectedFile.exists()) {
                load(selectedFile.toPath());
                restoreGUI(); // restores the GUI to reflect the state in which the loaded game was last saved
            }
        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));

        setGameState(GameState.INITIALIZED_UNMODIFIED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gameworkspace.reinitialize();
        gamedata = (GameData) appTemplate.getDataComponent();
    }

    public void hint(){
        char[] targetword = gamedata.getTargetWord().toCharArray();
        int charNumber = (int)(Math.random() * (targetword.length));



        int count = 0;

        while(count<targetword.length&&targetword[charNumber]==blanks[charNumber].getText().charAt(0)){
            charNumber = (charNumber+1)%targetword.length;
            count++;
        }

        //same as adding a good guess
        char guess = targetword[charNumber];

//        System.out.println("hint: "+guess);
        alphabet[guess-'a'].setStrikethrough(true); // strikethrough when you do shit
//        alphabet[guess-'a'].setOpacity(.3); // make it harder to see when already chosen
        javafx.scene.paint.Color p = javafx.scene.paint.Color.rgb(0, 200, 0); // make it green?
        alphabet[guess-'a'].setFill(p);
//                        alphabet[guess-'a'].set
        for (int i = 0; i < progress.length; i++) {
            if (gamedata.getTargetWord().charAt(i) == guess) {
                blanks[i].setText(progress[i].getText());
                gamedata.addGoodGuess(guess);
                discovered++;
            }
        }

        //success stuff
        gamedata.decrementRemaining();
        setGameState(GameState.INITIALIZED_MODIFIED);
        success = (discovered == progress.length);

        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.updateHangman(gamedata.getRemainingGuesses());

    }





}