package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.*;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;

import static hangman.HangmanProperties.*;

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Christopher Delvalle
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    Label             guiHeadingLabel;   // workspace (GUI) heading label
    HBox              headPane;          // conatainer to display the heading
    HBox              bodyPane;          // container for the main game displays
    ToolBar           footToolbar;       // toolbar for game buttons
    BorderPane        figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox              gameTextsPane;     // container to display the text-related parts of the game
    HBox              guessedLetters;    // text area displaying all the letters guessed so far
    HBox              remainingGuessBox; // container to display the number of remaining guesses
    Button            startGame;         // the button to start playing a game of Hangman
    HangmanController controller;

    Button hintButton; //new button for hints

    BorderPane borderPane;

    //body parts
    Circle head = new Circle();
    Rectangle rope = new Rectangle();
    Rectangle topBar = new Rectangle();
    Rectangle sideBar = new Rectangle();
    Rectangle bottomBar = new Rectangle();
    Rectangle body = new Rectangle();
    Polygon leftLeg = new Polygon();
    Polygon rightLeg = new Polygon();
    Rectangle rightArm = new Rectangle();
    Rectangle leftArm = new Rectangle();
    //


    VBox vBox;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (HangmanController) gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
    }

    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));

        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);

        figurePane = new BorderPane();
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();

        //>>>>
        hintButton = new Button("Hint");

        hintButton.setVisible(false);
        //<<<<

        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters, hintButton);


        bodyPane = new HBox();
        bodyPane.getChildren().addAll(figurePane, gameTextsPane);

        startGame = new Button("Start Playing");
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight);
//        figurePane.setRight(footToolbar); // wrong ************

//BUILDING THE HANGMAN!


        head.setRadius(45);
        head.setCenterX(170);
        head.setCenterY(80);
        head.setVisible(false);


        rope.setHeight(65);
        rope.setWidth(15);
        rope.setY(10);
        rope.setX(162);
        rope.setVisible(false);

        topBar.setWidth(167);
        topBar.setHeight(15);
        topBar.setY(10);
        topBar.setX(10);
        topBar.setVisible(false);

        sideBar.setHeight(340);
        sideBar.setWidth(15);
        sideBar.setX(10);
        sideBar.setY(10);
        sideBar.setVisible(false);

        bottomBar.setHeight(15);
        bottomBar.setWidth(240);
        bottomBar.setX(10);
        bottomBar.setY(350);
        bottomBar.setVisible(false);

        body.setHeight(160);
        body.setWidth(15);
        body.setY(80);
        body.setX(162);
        body.setVisible(false);

        leftLeg.getPoints().addAll(169.0, 240.0,
                169.0, 225.0,
                100.0, 280.0,
                120.0, 280.0);
        leftLeg.setVisible(false);

        rightLeg.getPoints().addAll(169.0, 240.0,
                169.0, 225.0,
                238.0, 280.0,
                218.0, 280.0);
        rightLeg.setVisible(false);

        rightArm.setHeight(12);
        rightArm.setWidth(75);
        rightArm.setX(169);
        rightArm.setY(150);
        rightArm.setVisible(false);

        leftArm.setHeight(12);
        leftArm.setWidth(75);
        leftArm.setX(94);
        leftArm.setY(150);
        leftArm.setVisible(false);

//        vBox = new VBox();
//        vBox.getChildren().addAll(headPane,bodyPane);
        Pane pane = new Pane();
        pane.getChildren().addAll(head,rope,topBar,sideBar,bottomBar,body,leftLeg,rightLeg, leftArm, rightArm);


        //Hangman built!...

        borderPane = new BorderPane();


// I'm not sure **********
//        vBox.getChildren().addAll(circle);
        workspace = new BorderPane();

        ((BorderPane)workspace).setTop(headPane);
        ((BorderPane)workspace).setRight(bodyPane);
        ((BorderPane)workspace).setLeft(pane);
        ((BorderPane)workspace).setBottom(footToolbar);

//        workspace.getChildren().addAll(borderPane);//headPane, footToolbar,bodyPane);
    }

    private void setupHandlers() {
        startGame.setOnMouseClicked(e -> controller.start());


    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }//???

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }

    public Button getStartGame() {
        return startGame;
    }

    public Button getHintButton() {return hintButton;}

    public void reinitialize() {
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();
        hintButton = new Button("Hint");
        hintButton.setVisible(false);

        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters,hintButton);
        bodyPane.getChildren().setAll(figurePane, gameTextsPane);
    }

    public void updateHangman(int remaining){
        switch(remaining){
            case 0:
                rightLeg.setVisible(true);
            case 1:
                rightArm.setVisible(true);
            case 2:
                leftLeg.setVisible(true);
            case 3:
                leftArm.setVisible(true);
            case 4:
                body.setVisible(true);
            case 5:
                head.setVisible(true);
            case 6:
                rope.setVisible(true);
            case 7:
                topBar.setVisible(true);
            case 8:
                sideBar.setVisible(true);
            case 9:
                bottomBar.setVisible(true);
                break;
            default:
                bottomBar.setVisible(false);
                sideBar.setVisible(false);
                topBar.setVisible(false);
                rope.setVisible(false);
                head.setVisible(false);
                body.setVisible(false);
                leftArm.setVisible(false);
                leftLeg.setVisible(false);
                rightArm.setVisible(false);
                rightLeg.setVisible(false);
                break;

        }
    }
}